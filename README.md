[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 4

Problemas propuestos para la Sesión 4 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

En estos ejercicios se utilizarán las interfaces [`Executors`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas. Las tareas se definirán como clases que implementen la interface [`Callable<V>`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Callable.html). 


Los **objetivos** de la práctica son:

-   Definir adecuadamente las clases que implementan lo que se ejecutarán en los marcos de ejecución. Programando adecuadamente la excepción de interrupción de su ejecución.
- Crear los marcos de ejecución que se necesiten y añadir las tareas para su ejecución así como la sincronización que se pida utilizando las capacidades del marco de ejecución para ello.
-   Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.
    -   Realizará la prueba de las tareas definidas en el ejercicio y la finalización correcta de los diferentes marcos de ejecuciónn que sean necesarios.
    - Debe garantizar que todas las tareas están finalizadas antes de dar por terminada su ejecución.


Los ejercicios son diferentes para cada grupo:

-   [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion4/-/blob/master/README.md#grupo-1)
-   [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion4/-/blob/master/README.md#grupo-2)
-   [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion4/-/blob/master/README.md#grupo-3)
-   [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion4/-/blob/master/README.md#grupo-4)
-   [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion4/-/blob/master/README.md#grupo-5)
-   [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion4/-/blob/master/README.md#grupo-6)

### Grupo 1
Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `Ordenador`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

 - `CrearProcesos` : Esta tarea se encargará de ir creando un proceso y tiene como variables de instancia:
	 - Un identificador.
	 - Un `TipoProceso` asociado al proceso que crea.

	La tarea que tiene que realizar es:
	- Crea un proceso
	- Se simulará un tiempo de creación definido por las constantes.
	- El resultado de su ejecución es el proceso que ha creado.
	- Debe programarse la interrupción de la tarea de creación.

- `GestorMemoria` : Es el encargado de asignar un `Proceso` a un ordenador de los que tiene asignados. La clase tiene las siguientes variables de instancia:
	-   Un identificador

	La tarea que debe realizar es la siguiente:
	- Crea una lista de ordenadores a la que tendrá que asignar los procesos.
	- Crea una lista para añadir los procesos no asignados.
	- Mientras no se alcance el número de fallos establecido:
		- Crear un número aleatorio de `CrearProceso` y lo añade a un marco para su ejecución.
		- Espera a que finalicen todas las tareas antes de empezar la asignación de los procesos.
		- Ciclo de asignación de procesos hasta que se haya alcanzado el máximo de fallos establecido:
			- Para cada `Proceso` se buscará un ordenador donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre los diferentes ordenadores. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
			- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción completando la última asignación de proceso.
	- El resultado de la ejecución de esta tarea es crear un `Informe` donde se debe mostrar el estado de asignación de procesos a los ordenadores y los procesos no asignados. Si se ha interrumpido la ejecución de la tarea debe quedar reflejado en el informe.

- `Informe` : Es el resultado de la ejecución de una tarea `GestorMemoria`.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Crea un número de gestores de memoria que estarán determinados por una constante.
	- Añadir los gestores al marco de ejecución y esperar a que finalice uno.
	- Solicita la interrupción del resto de gestores.
	- Finaliza el marco de ejecución.
	- Presentará el informe de cada uno de los gestores.

### Grupo 2
Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `ColaPrioridad`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

 - `CrearProcesos` : Esta tarea se encargará de ir creando procesos y tiene las siguientes variables de instancia:
	 - Un identificador.

	La tarea que tiene que realizar es:
	 - Se crea un `Proceso` con un tipo de prioridad aleatorio y se simulará un tiempo de creación utilizando las constantes.
	 - Se debe programar la interrupción de la tarea si es solicitada.
	 - El resultado de la tarea es el proceso que se ha creado.

- `GestorProcesos` : Es el encargado de asignar un `Proceso` a una cola de prioridad. La clase tiene las siguientes variables de instancia:
	-   Un identificador

	La tarea que debe realizar es la siguiente:
	- Crear una lista de colas de prioridad a las que tiene acceso el gestor.
	- Crear una lista de procesos no asignados.
	- Mientras no se alcance el número de fallos establecido.
		- Crear un número variable de tareas `CrearProcesos` y se añaden a su marco de ejecución.
		- Esperará hasta que todos las tareas de creación hayan finalizado.
		- Para cada uno de los procesos creados se buscará una cola de prioridad donde asignarlo, hay que procurar que la carga de procesos esté balanceada. Se simulará un tiempo aleatorio para la realización de esta operación mediante las constantes.
		- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción finalizando las tareas `CrearProcesos` asociadas.
	- El resultado de la tarea es el informe del estado de sus colas de prioridad y los procesos no asignados. Si la tarea ha sido interrumpida deberá quedar reflejado en el informe.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Crea un número de gestores que estarán determinados por una constante.
	- Añadir los gestores al marco de ejecución y esperar a que finalice uno.
	- Solicita la interrupción del resto de gestores.
	- Finaliza el marco de ejecución.
	- Presentará el informe de cada uno de los gestores.

### Grupo 3

Para la realización del ejercicio se hará uso de las clases ya definidas: `Archivo`, `UnidadAlmacenamiento`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

 - `CrearArchivoss` : Esta tarea se encargará de ir creando un archivo y tiene como variable de instancia:
	 - Un identificador.

	La tarea que tiene que realizar es:
	- Crea un archivo.
	- Se simulará un tiempo de creación definido por las constantes.
	- El resultado de su ejecución es el archivo que ha creado.
	- Debe programarse la interrupción de la tarea de creación.

- `GestorAlmacenamiento` : Es el encargado de asignar un `Archivo` a una unidad de almacenamiento que tiene asignadas. La clase tiene las siguientes variables de instancia:
	-   Un identificador

	La tarea que debe realizar es la siguiente:
	- Crea una lista de unidades de almacenamiento a la que tendrá que asignar los archivos.
	- Crea una lista para añadir los archivos no asignados.
	- Mientras no se alcance un número de archivos almacenados establecido:
		- Crear un número aleatorio de `CrearArchivo` y lo añade a un marco para su ejecución.
		- Espera a que finalicen todas las tareas antes de empezar la asignación de los archivos.
		- Ciclo de asignación de archivos hasta que se haya alcanzado el máximo de archivos establecido:
			- Para cada `Archivo` se buscará la primera unidad de almacenamiento donde asignarlo. Se simulará un tiempo aleatorio, atendiendo al tipo de archivo, para la realización de esta operación.
			- Si no ha podido ser asignado el archivo se añadirá a la lista de archivos no asignados.
	- Programar la interrupción completando la última asignación de archivo.
	- El resultado de la ejecución de esta tarea es crear un `Informe` donde se debe mostrar el estado de asignación de los archivos y los archivos no asignados. Si se ha interrumpido la ejecución de la tarea debe quedar reflejado en el informe.

- `Informe` : Es el resultado de la ejecución de una tarea `GestorAlmacenamiento`.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Crea un número de gestores de almacenamiento que estarán determinados por una constante.
	- Añadir los gestores al marco de ejecución y esperar a que finalice uno.
	- Solicita la interrupción del resto de gestores.
	- Finaliza el marco de ejecución.
	- Presentará el informe de cada uno de los gestores.

## Grupo 4

Se ha decidido que la forma anterior de trabajar con la gestión de los menús no era eficiente. Para optimizar el tiempo, primero los `restaurantes` generarán todos los `platos` necesarios, para que posteriormente los `repartidores` puedan rellenar los `MenuReparto` disponibles. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten. En este primer ejercicio con marcos de ejecución no se cancelan los hilos.
- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
	- **Nota**: los tiempos de espera se representan en milisegundos.
- `Plato`: Clase que representa a un plato de tipo `TipoPlato`.
- `MenuReparto`: Representa a un pedido, puede incluir un plato de cada uno de los presentes en `TipoPlato` pero no más de uno de cada tipo.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Restaurante`:
	- Los restaurantes guardarán en variables de instancia, su identificador, el `TipoPlato` que puede producir, un array para los platos producidos y un elemento de sincronización para guardar los platos en el array. 
	- Cada tipo de restaurante fabricará platos de su tipo hasta que se alcancen `TOTAL_PLATOS_POR_TIPO` guardados en el array común.
	- Entre la generación de cada plato, el restaurante esperará entre `TIEMPO_ESPERA_MIN` y `TIEMPO_ESPERA_MAX` milisegundos.
- `Repartidor`:
	- El repartidor guardará en variables de instancia, su identificador, un array de pedidos pendientes, un array para cada `TipoPlato` producidos y un elemento de sincronización para cada uno de los array.
	- Por pedido asignado:
		- Insertar un plato de cada tipo en el pedido.
		- Esperar entre `TIEMPO_ESPERA_MIN` y `TIEMPO_ESPERA_MAX` milisegundos.
	- Al finalizar, imprimirá los datos de los pedidos asignados.
- `Hilo Principal`:
	- Generará un array y un ReentrantLock por cada tipo de plato.
	- Generar el gestor/ejecutor de tareas.
	- Crea e inicia la ejecución de `RESTAURANTES_A_GENERAR_POR_TIPO` de cada tipo de plato. En total habrá `RESTAURANTES_A_GENERAR_POR_TIPO` * `TOTAL_TIPOS_PLATOS` restaurantes.
	- Hacer un `shutdown()` del gestor de hilos y esperar a que acaben con `awaitTermination()`.
	- Generar un nuevo gestor/ejecutor de tareas para los repartidores.
	- Crea e inicia la ejecución de `REPARTIDORES_A_GENERAR` repartidores con `MENUS_A_GENERAR` menús asignados.


## Grupo 5

Se ha decidido que la forma anterior de trabajar con la gestión de la impresión y postprocesado de modelos no era eficiente. Para optimizar el tiempo, primero los `ingenieros` generarán todos los modelos necesarios, y al cabo de un rato una serie de `MáquinaPostprocesado` se lanzarán para procesar los modelos, durante un rato la ejecución de los ingenieros y maquinas se solapará. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten. En este primer ejercicio con marcos de ejecución no se cancelan los hilos.
- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
	- **Nota**: los tiempos de espera se representan en milisegundos.
- `Modelo`: Clase que representa a un modelo 3D con unos requisitos de tipo `CalidadImpresion`.
- `Impresora3D`: Representa a una impresora 3D y una `CalidadImpresion` que puede generar la impresora. 

El ejercicio consiste en completar la implementación de las siguientes clases:

 - `MaquinaPostprocesado`:
   	-  Las máquinas guardarán en variables de instancia, su identificador, un array de modelos impresos, un array de modelos procesados, y un `ReentrantLock` para cada array.
   	- Mientras queden modelos en el array de modelos impresos:
   		- Sacar un modelo del array de modelos impresos. 
   			- **Nota**: volver a comprobar dentro de la exclusión mutua que realmente quedan modelos.
   		- Esperar `TIEMPO_ESPERA_MAQUINA` milisegundos.
   		- Introducir el modelo en el array de modelos procesados.
	- Al terminar se hará un return de los modelos procesados.
- `Ingeniero`:
	- Las máquinas guardarán en variables de instancia, su identificador, una impresora, un array de modelos impresos y un `ReentrantLock` para proteger al array de escrituras simultaneas.
	- Imprimirá entre `MODELOS_A_GENERAR_MIN` y `MODELOS_A_GENERAR_MAX` modelos:
		- La calidad de los modelos vendrá determinada por la calidad de la impresora asignada.
		- Después de crear el modelo y guardarlo en el array de modelos impresos, el ingeniero esperará `TIEMPO_ESPERA_INGENIERO` milisegundos.	
	- Al terminar se hará un return de los modelos impresos.
- `Hilo Principal`:
	- Generará los dos arrays de modelos y un ReentrantLock para cada uno.
	- Generará las estructuras para guardar el resultado de los ingenieros y máquinas
	- Generar el gestor/ejecutor de tareas.
	- Crea e inicia la ejecución de `INGENIEROS_A_GENERAR` ingenieros. 
	- El hilo principal esperará `TIEMPO_ESPERA_PARA_CREAR_MAQUINAS` milisegundos.
	- Crea e inicia la ejecución de `MAQUINAS_A_GENERAR` máquinas de post procesado.
	- Hacer un `shutdown()` del gestor de hilos y esperar a que acaben con `awaitTermination()`.
	- Mostrar por pantalla cuantos modelos han sido impresos y cuantos han sido procesados en total.


## Grupo 6

Se ha decidido que la forma anterior de trabajar con la gestión de la vacunación no era eficiente. Para optimizar el tiempo, primero los `almacenes médicos` generarán todos las `dosisVacuna` necesarias, para que posteriormente los `enfermeros` puedan vacunar a los pacientes disponibles. Como contrapartida, un almacén solo puede suministrar dosis de un fabricante concreto y cada enfermero está entrenado en inyectar un tipo de vacuna concreto. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten. En este primer ejercicio con marcos de ejecución no se cancelan los hilos.
- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
	- **Nota**: los tiempos de espera se representan en milisegundos.
- `DosisVacuna`: Clase que representa a cada dosis de la vacuna de un `FabricanteVacuna` determinado.
- `Paciente`: Representa a un paciente al que se le pueden administrar hasta dos dosis siempre que las dos sean del mismo fabricante.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `AlmacénMédico`:
	- Los almacenes guardarán en variables de instancia, su identificador, el `FabricanteVacuna` que puede producir, un array para las dosis producidas y un elemento de sincronización para guardar los platos en el array. 
	- Cada tipo de almacén fabricará dosis de su tipo hasta que se alcancen `TOTAL_DOSIS_A_GENERAR` guardados en el array común.
	- Entre la generación de cada dosis, el almacén esperará `TIEMPO_ESPERA_ALMACEN` milisegundos.
- `Enfermero`:
	- El enfermero guardará en variables de instancia, su identificador, un array de pacientes, un array con las `DosisVacuna` producidos y un elemento de sincronización el array. Los enfermeros solo podrán inyectar dosis del tipo asignado.
	- Por paciente asignado y dosis, cada paciente recibe dos:
		- Buscará una dosis que coincida con el tipo que puede inyectar. Si no hay dosis disponibles de ese tipo dejará de vacunar a los pacientes restantes
		- Esperar `TIEMPO_ESPERA_ENTRE_DOSIS` milisegundos.
	- Al finalizar, imprimirá los datos de los pacientes asignados.
- `Hilo Principal`:
	- Generará un array de dosis y un ReentrantLock.
	- Generar el gestor/ejecutor de tareas.
	- Crea e inicia la ejecución de `ALMACENES_A_GENERAR`. Se tiene que crear un tercio de almacenes de cada tipo de fabricante.
	- Hacer un `shutdown()` del gestor de hilos y esperar a que acaben con `awaitTermination()`.
	- Generar un nuevo gestor/ejecutor de tareas para los enfermeros.
	- Crea e inicia la ejecución de `ENFERMEROS_A_GENERAR` enfermeros con sus pacientes, entre `PACIENTES_A_GENERAR_MIN`  y `PACIENTES_A_GENERAR_MIN`. Se tiene que crear un tercio de enfermeros de cada tipo de fabricante.
