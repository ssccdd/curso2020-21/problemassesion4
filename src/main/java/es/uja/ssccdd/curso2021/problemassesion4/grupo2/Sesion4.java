/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Constantes.TIEMPO_ESPERA;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion4 {
    static int ID_SISTEMA = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService ejecucion;
        Informe informe;
        ArrayList<GestorProcesos> listaGestores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        ejecucion = Executors.newFixedThreadPool(NUM_GESTORES);
        listaGestores = new ArrayList();
        
        // Creamos los gestores
        for(int i = 0; i < NUM_GESTORES; i++) {
            GestorProcesos gestor = new GestorProcesos("GESTOR(" + i + ")");
            listaGestores.add(gestor);
        }
        
        // Ejecutamos los gestores y esperamos a que finalice el primero
        System.out.println("(HILO_PRINCIPAL) Espera a la finalización de un gestor");
        informe = ejecucion.invokeAny(listaGestores);
        
        // Se solicita la cancelación de los gestores que no han terminado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización de los gestores");
        ejecucion.shutdown();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Mostrar el estado de sistema distribuido
        System.out.println("(HILO_PRINCIPAL) Estado del gestor que ha finalizado\n" +
                            informe);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
        
    }
    
    public static int generaID_SISTEMA() {
        return ID_SISTEMA++;
    }
}
