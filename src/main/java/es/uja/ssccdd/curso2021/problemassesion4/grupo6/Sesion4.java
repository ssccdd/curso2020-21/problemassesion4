/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo6;

import es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.PACIENTES_A_GENERAR_MAX;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.PACIENTES_A_GENERAR_MIN;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.ENFERMEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.ALMACENES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.TOTAL_TIPOS_FABRICANTES;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */


public class Sesion4 {

    public static void main(String[] args) {

        // Variables aplicación
        int idPacientes = 0;
        ReentrantLock cerrojoDosis = new ReentrantLock();
        ArrayList<DosisVacuna> dosis = new ArrayList<>();

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando almacenes");

        for (int i = 0; i < ALMACENES_A_GENERAR; i++) {

            FabricanteVacuna fabricante = FabricanteVacuna.getFabricanteOrdinal(i % TOTAL_TIPOS_FABRICANTES);

            executor.execute(new AlmacenMedico(i, fabricante, dosis, cerrojoDosis));

        }

        executor.shutdown();

        System.out.println("HILO-Principal Esperando a los almacenes");
        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            //Los hilos deben de terminar antes
            Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Generando enfermeros");
        //Hace falta un nuevo executor despues de parar al anterior       
        executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

        for (int i = 0; i < ENFERMEROS_A_GENERAR; i++) {

            // Inicializamos los pacientes
            ArrayList<Paciente> listaPacientes = new ArrayList<>();
            int pacientesAGenerar = random.nextInt(PACIENTES_A_GENERAR_MAX - PACIENTES_A_GENERAR_MIN) + PACIENTES_A_GENERAR_MIN;
            for (int j = 0; j < pacientesAGenerar; j++) {
                listaPacientes.add(new Paciente(idPacientes++));
            }

            FabricanteVacuna fabricante = FabricanteVacuna.getFabricanteOrdinal(i % TOTAL_TIPOS_FABRICANTES);

            // Inicializamos los enfermeros
            executor.execute(new Enfermero(i, listaPacientes, dosis, fabricante, cerrojoDosis));

        }
        
        executor.shutdown();

        System.out.println("HILO-Principal Esperando a los enfermeros");
        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            //Los hilos deben de terminar antes
            Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        System.out.println("HILO-Principal Han quedado sin usar " + dosis.size() + " dosis.");

        for (DosisVacuna dosi : dosis) {
            System.out.println("\t" + dosi);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
