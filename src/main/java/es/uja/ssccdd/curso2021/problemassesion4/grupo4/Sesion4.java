/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.MENUS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TOTAL_TIPOS_PLATOS;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.REPARTIDORES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.RESTAURANTES_A_GENERAR_POR_TIPO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion4 {

    public static void main(String[] args) {
        // Variables aplicación
        int idMenus = 0;
        int idRepartidores = 0;
        int idRestaurante = 0;
        
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        
        ArrayList<ReentrantLock> cerrojos = new ArrayList<>();
        ArrayList<ArrayList<Plato>> platos = new ArrayList<>();
                
        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");
        
        //Inicializo un array para cada tipo de plato y un cerrojo para cada array
        for(int i = 0 ; i < TOTAL_TIPOS_PLATOS;i++){
        
            cerrojos.add(new ReentrantLock());
            platos.add(new ArrayList<>());
            
        }
        
        
        System.out.println("HILO-Principal Generando restaurantes");
        
        for(int i = 0 ; i < TOTAL_TIPOS_PLATOS;i++){
        
            for (int j = 0; j < RESTAURANTES_A_GENERAR_POR_TIPO; j++) {
                
                executor.execute(new Restaurante(idRestaurante++, Utils.TipoPlato.getPlatoOrdinal(i), platos.get(i), cerrojos.get(i)));
                
            }
            
        }
        
        executor.shutdown();
        
        System.out.println("HILO-Principal Esperando a los restaurantes");
        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            //Los hilos deben de terminar antes
            Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("HILO-Principal Generando restaurantes");
        
        executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        
        for (int i = 0; i < REPARTIDORES_A_GENERAR; i++) {

            // Inicializamos los menús
            ArrayList<MenuReparto> listaPedidos = new ArrayList<>();
            for (int j = 0; j < MENUS_A_GENERAR; j++) {
                listaPedidos.add(new MenuReparto(idMenus++));
            }

            // Inicializamos los repartidores
            executor.execute(new Repartidor(idRepartidores++, listaPedidos, cerrojos, platos));

        }

        executor.shutdown();
        
        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
