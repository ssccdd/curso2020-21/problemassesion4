/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo5;

import es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.CalidadImpresion;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Impresora3D {

    private final int iD;
    private final CalidadImpresion calidadImpresion;

    /**
     * Genera una nueva impresora 3D
     *
     * @param iD id de la impresora
     * @param calidadMaxima calidad de impresión que puede alcanzar la impresora
     */
    public Impresora3D(int iD, CalidadImpresion calidadMaxima) {
        this.iD = iD;
        this.calidadImpresion = calidadMaxima;
    }

    public int getiD() {
        return iD;
    }

    public CalidadImpresion getCalidadImpresion() {
        return calidadImpresion;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder resultado = new StringBuilder();

        resultado.append("Impresora[").append(iD).append(", calidad=").append(calidadImpresion).append("]");

        return resultado.toString();
    }

}
