/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo5;

import es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.INGENIEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.MAQUINAS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.TIEMPO_ESPERA_PARA_CREAR_MAQUINAS;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion4 {

    public static void main(String[] args) throws InterruptedException {

        ArrayList<Thread> hilosIngenieros = new ArrayList<>();
        ArrayList<Modelo> modelosImpresos = new ArrayList<>();
        ArrayList<Modelo> modelosFinalizados = new ArrayList<>();
        ReentrantLock bloqueoModelosImpresos = new ReentrantLock();
        ReentrantLock bloqueoModelosFinalizados = new ReentrantLock();
        ArrayList<Future<Integer>> resultadosIngenieros = new ArrayList<>();
        ArrayList<Future<Integer>> resultadosMaquinas = new ArrayList<>();

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");
        System.out.println("HILO-Principal Generando ingenieros");

        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {

            int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
            Impresora3D impr = new Impresora3D(i, CalidadImpresion.getCalidad(aleatorioCalidad));
            // Inicializamos los ingenieros
            Ingeniero ingeniero = new Ingeniero(i, impr, modelosImpresos, bloqueoModelosImpresos);
            resultadosIngenieros.add(executor.submit(ingeniero));

        }

        System.out.println("HILO-Principal Espera para iniciar las máquinas");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_PARA_CREAR_MAQUINAS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Generando máquinas");

        for (int i = 0; i < MAQUINAS_A_GENERAR; i++) {

            // Inicializamos los ingenieros
            MaquinaPostProcesado maquina = new MaquinaPostProcesado(i, modelosImpresos, modelosFinalizados, bloqueoModelosImpresos, bloqueoModelosFinalizados);
            resultadosMaquinas.add(executor.submit(maquina));

        }

        System.out.println("HILO-Principal Espera a ingenieros y máquinas");

        executor.shutdown();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
        }

        int contadorImpresos = 0;
        int contadorProcesados = 0;

        try {

            for (int i = 0; i < resultadosIngenieros.size(); i++) {

                contadorImpresos += resultadosIngenieros.get(i).get();

            }

            for (int i = 0; i < resultadosMaquinas.size(); i++) {

                contadorProcesados += resultadosMaquinas.get(i).get();
            }

        } catch (Exception ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("HILO-Principal Impresos " + contadorImpresos + " modelos.");
        System.out.println("HILO-Principal Procesados " + contadorProcesados + " modelos.");

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
