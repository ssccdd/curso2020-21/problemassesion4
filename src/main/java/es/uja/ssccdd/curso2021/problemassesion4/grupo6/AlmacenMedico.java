/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.TOTAL_DOSIS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.random;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.TIEMPO_ESPERA_ALMACEN;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico implements Runnable {

    private final int iD;
    private final FabricanteVacuna fabricante;
    private final ArrayList<DosisVacuna> dosis;
    private final ReentrantLock cerrojoDosis;

    public AlmacenMedico(int iD, FabricanteVacuna fabricante, ArrayList<DosisVacuna> dosis, ReentrantLock cerrojoDosis) {
        this.iD = iD;
        this.fabricante = fabricante;
        this.dosis = dosis;
        this.cerrojoDosis = cerrojoDosis;
    } 

    @Override
    public void run(){
        
         System.out.println("Almacén " + iD + " ha empezado.");

        while (dosis.size() < TOTAL_DOSIS_A_GENERAR) {

            cerrojoDosis.lock();
            {
                if (dosis.size() < TOTAL_DOSIS_A_GENERAR) {
                    //Doble check en región crítica para asegurarse que hay hueco
                    dosis.add(new DosisVacuna(Math.abs(random.nextInt()), fabricante));
                }
            }
            cerrojoDosis.unlock();

            try {
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_ALMACEN);
            } catch (InterruptedException ex) {
                //Los hilos no van a ser interrumpidos
                Logger.getLogger(AlmacenMedico.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.out.println("Almacén " + iD + " ha terminado.");
        
    }
    
}
