/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.ESPACIO_INSUFICIENTE;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.MAXIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.MINIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.NUM_FALLOS;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.TIEMPO_ESPERA;
import es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.TipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Sesion4.generaID_SISTEMA;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorAlmacenamiento implements Callable<Informe> {
    private final String iD;
    private final ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento;
    private final ArrayList<Archivo> noAsignados;
    private int fallosAsignacion;
    private final ExecutorService ejecucion;
    private List<Future<Archivo>> listaArchivosCreados;

    public GestorAlmacenamiento(String iD) {
        this.iD = iD;
        this.listaDeAlmacenamiento = new ArrayList();
        this.noAsignados = new ArrayList();
        this.fallosAsignacion = 0;
        
        // Marco ejecución para crear los procesos
        this.ejecucion = Executors.newCachedThreadPool();
    }
    
    

    @Override
    public Informe call() throws Exception {
        Informe informe = new Informe(iD);
        
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        inicializaGestor();
        
        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while( fallosAsignacion < NUM_FALLOS ) {
                crearArchivos();
                
                asignarArchivos();
            }
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException | ExecutionException ex) {
            informe.setCancelado(true);
            System.out.println("Se ha interrumpido la ejecución del TAREA-" + iD + ex);
        } finally {
            // Completa la finbalización del gestor
            finalizacion(informe);
            // Finalizamos la ejecución de tareas pendientes
            ejecucion.shutdownNow();
            ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        }
        
        return informe;
    }

    public String getiD() {
        return iD;
    }
    
    private void inicializaGestor() {
        int capacidad;
        
        int totalUnidadesAlmacenamiento = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        for(int j = 0; j < totalUnidadesAlmacenamiento; j++) {
            // Generamos la capacidad de la unidad de almacenamiento
            capacidad = MINIMO_ALMACENAMIENTO + aleatorio.nextInt(MAXIMO_ALMACENAMIENTO - 
                                                                      MINIMO_ALMACENAMIENTO + 1);
            
            listaDeAlmacenamiento.add(new UnidadAlmacenamiento(generaID_SISTEMA(),capacidad));
        }
    }
    
    private void crearArchivos() throws InterruptedException {
        ArrayList<CrearArchivo> listaTareas;
        
        // Creamos las tareas para la obtener los procesos
        int numTareas = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        listaTareas = new ArrayList();
        for(int i = 0; i < numTareas; i++) {
            CrearArchivo tarea = new CrearArchivo(iD + "-tarea para proceso-" + i);
            listaTareas.add(tarea);
        }
        
        // Ejecutamos las tareas y esperamos hasta que finalicen todas
        listaArchivosCreados = ejecucion.invokeAll(listaTareas);
    }
    
    private void asignarArchivos() throws InterruptedException, ExecutionException {
        // Asignamos los archivos a las unidades de almacenamiento
        Iterator it = listaArchivosCreados.iterator();
        while( it.hasNext() && (fallosAsignacion < NUM_FALLOS)) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de archivo.
            if ( Thread.interrupted() )
                throw new InterruptedException();
            
            Future<Archivo> archivoCreado = (Future<Archivo>) it.next();
            it.remove(); // Eliminamos el archivo de la lista
            Archivo archivo = archivoCreado.get();
            System.out.println("TAREA-" + iD + " Asignando " + archivo + " faltan " + listaArchivosCreados.size());
                
            // Asignamos el archivo a la primera unidad de almacenamiento disponible
            boolean asignado = false;
            int indice = 0;
            while ((indice < listaDeAlmacenamiento.size()) && !asignado) {
                if (listaDeAlmacenamiento.get(indice).addArchivo(archivo) != ESPACIO_INSUFICIENTE) {
                    asignado = true;
                } else 
                    indice++;
            }

            // Si no se ha podido almacenar el archivo se anota
            if(!asignado) {
                noAsignados.add(archivo);
                fallosAsignacion++;
            }

            // Simulamos por último el tiempo de asignación porque si se interrumpe
            // ya hemos completado la operación de asignación
            TimeUnit.SECONDS.sleep(tiempoAsignacion(archivo.getTipoArchivo()));
        }
    }
    
    private void finalizacion(Informe informe) {
        // registramos los archivos no asignados
        try {
            for(Future<Archivo> archivoCreado : listaArchivosCreados)
                noAsignados.add(archivoCreado.get());

        } catch (InterruptedException | ExecutionException ex) {
            // No hacemos nada porque se está finializando
        } finally {
           informe.setListaDeAlmacenamiento(listaDeAlmacenamiento);
           informe.setListaNoAsignados(noAsignados);
        }
    }
    
    private int tiempoAsignacion(TipoArchivo tipoArchivo) {
        return MAXIMO + tipoArchivo.ordinal();
    }
    
}
