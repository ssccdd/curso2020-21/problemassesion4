/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo4;

import es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TipoPlato;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Plato {

    private final int iD;
    private final TipoPlato tipo;

    public Plato(int iD, TipoPlato plato) {
        this.iD = iD;
        this.tipo = plato;
    }

    public int getiD() {
        return iD;
    }

    public TipoPlato getTipo() {
        return tipo;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        return "Plato{" + "iD= " + iD + ", tipo de plato= " + tipo + '}';
    }

}
