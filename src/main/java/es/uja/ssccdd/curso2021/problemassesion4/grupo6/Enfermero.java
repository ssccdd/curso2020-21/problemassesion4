/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo6;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.TIEMPO_ESPERA_ENTRE_DOSIS;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.DOSIS_POR_PACIENTE;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.FabricanteVacuna;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Enfermero implements Runnable {

    private final int iD;
    private final ArrayList<Paciente> pacientes;
    private final ArrayList<DosisVacuna> dosis;
    private final FabricanteVacuna fabricante;
    private final ReentrantLock cerrojoDosis;

    public Enfermero(int iD, ArrayList<Paciente> pacientes, ArrayList<DosisVacuna> dosis, FabricanteVacuna fabricante, ReentrantLock cerrojoDosis) {
        this.iD = iD;
        this.pacientes = pacientes;
        this.dosis = dosis;
        this.fabricante = fabricante;
        this.cerrojoDosis = cerrojoDosis;
    }

    @Override
    public void run() {

        boolean hayMasDosisFabricante = true;

        for (int i = 0; i < pacientes.size() && hayMasDosisFabricante; i++) {

            for (int j = 0; j < DOSIS_POR_PACIENTE && hayMasDosisFabricante; j++) {

                DosisVacuna siguienteDosis = null;
                int indiceSiguienteDosis = -1;

                cerrojoDosis.lock();
                {
                    for (int k = 0; k < dosis.size() && indiceSiguienteDosis < 0; k++) {
                        if (dosis.get(k).getFabricante() == fabricante) {
                            indiceSiguienteDosis = k;
                        }
                    }
                    if (indiceSiguienteDosis >= 0) { //Hay dosis                                        
                        siguienteDosis = dosis.get(indiceSiguienteDosis);
                        dosis.remove(indiceSiguienteDosis);
                    }
                }
                cerrojoDosis.unlock();

                if (siguienteDosis != null) {
                    pacientes.get(i).addDosisVacuna(siguienteDosis);
                } else {
                    hayMasDosisFabricante = false;
                }

                try {
                    TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_ENTRE_DOSIS);
                } catch (InterruptedException ex) {
                    //Los hilos no van a ser interrumpidos
                    Logger.getLogger(AlmacenMedico.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

        imprimirInfo();

    }

    /**
     * Imprime una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    public void imprimirInfo() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nEnfermero ").append(iD);

        for (Paciente pac : pacientes) {
            mensaje.append("\n\t").append(pac.toString());
        }

        System.out.println(mensaje.toString());
    }

}
