/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo6;

import es.uja.ssccdd.curso2021.problemassesion4.grupo6.Utils.FabricanteVacuna;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class DosisVacuna {

    private final int iD;
    private final FabricanteVacuna fabricante;

    public DosisVacuna(int iD, FabricanteVacuna fabricante) {
        this.iD = iD;
        this.fabricante = fabricante;
    }

    public int getiD() {
        return iD;
    }

    public FabricanteVacuna getFabricante() {
        return fabricante;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        return "Vacuna{" + "iD= " + iD + ", fabricante= " + fabricante + "}";
    }

}
