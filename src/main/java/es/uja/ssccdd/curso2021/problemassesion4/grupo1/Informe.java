/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo1;

import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Informe {
    private final String gestor;
    private ArrayList<Ordenador> listaOrdenadores;
    private ArrayList<Proceso> listaNoAsignados;
    private boolean cancelado;

    public Informe(String gestor) {
        this.gestor = gestor;
        this.listaOrdenadores = null;
        this.listaNoAsignados = null;
        this.cancelado = false;
    }

    public Informe(String gestor, ArrayList<Ordenador> listaOrdenadores, ArrayList<Proceso> listaNoAsignados) {
        this.gestor = gestor;
        this.listaOrdenadores = listaOrdenadores;
        this.listaNoAsignados = listaNoAsignados;
        this.cancelado = false;
    }

    public void setListaOrdenadores(ArrayList<Ordenador> listaOrdenadores) {
        this.listaOrdenadores = listaOrdenadores;
    }

    public void setListaNoAsignados(ArrayList<Proceso> listaNoAsignados) {
        this.listaNoAsignados = listaNoAsignados;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }

    @Override
    public String toString() {
        String resultado = "******************** " + gestor + " ********************\n";
        
        for(Ordenador ordenador : listaOrdenadores)
                resultado = resultado + ordenador + "\n";
            
        resultado = resultado + "Procesos no asignados " + listaNoAsignados + "\n";

        if( cancelado )
            resultado = "El gestor ha sido CANCELADO\n";
        
        return resultado;
    }
   
}
