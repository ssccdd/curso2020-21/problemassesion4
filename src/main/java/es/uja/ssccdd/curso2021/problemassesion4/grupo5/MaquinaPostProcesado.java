/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.TIEMPO_ESPERA_MAQUINA;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MaquinaPostProcesado implements Callable<Integer> {

    private final int iD;
    private final ArrayList<Modelo> modelosImpresos;
    private final ArrayList<Modelo> modelosFinalizados;
    private final ReentrantLock bloqueoModelosImpresos;
    private final ReentrantLock bloqueoModelosFinalizados;

    public MaquinaPostProcesado(int id, ArrayList<Modelo> modelosImpresos, ArrayList<Modelo> modelosFinalizados, ReentrantLock bloqueoModelosImpresos, ReentrantLock bloqueoModelosFinalizados) {
        this.iD = id;
        this.modelosImpresos = modelosImpresos;
        this.modelosFinalizados = modelosFinalizados;
        this.bloqueoModelosImpresos = bloqueoModelosImpresos;
        this.bloqueoModelosFinalizados = bloqueoModelosFinalizados;
    }

    @Override
    public Integer call() throws Exception {

        System.out.println("Maquina de postprocesado " + iD + " iniciada");

        int modelosProcesados = 0;

        while (!modelosImpresos.isEmpty()) {

            Modelo aProcesar = null;

            bloqueoModelosImpresos.lock();
            { //Las llaves no son obligatorias, solo las uso sirven para resaltar la sección crítica
                if (!modelosImpresos.isEmpty()) { 
                //Doble check en sección crítica para evitar, por ejemplo, que dos hilos pasen el while, pero solo haya un modelo
                    aProcesar = modelosImpresos.get(0);
                    modelosImpresos.remove(0);
                }

            }
            bloqueoModelosImpresos.unlock();

            if (aProcesar != null) {
                try {
                    TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_MAQUINA);
                } catch (InterruptedException ex) {
                    //Este hilo no se interrumpirá por lo que no necesita tratamiento de la excepción
                }

                bloqueoModelosFinalizados.lock();
                {
                    modelosFinalizados.add(aProcesar);
                }
                bloqueoModelosFinalizados.unlock();

                modelosProcesados++;
            }
        }

        System.out.println("Maquina de postprocesado " + iD + " finalizada\tTotal modelos procesados: " + modelosProcesados);

        return modelosProcesados;
    }

}
