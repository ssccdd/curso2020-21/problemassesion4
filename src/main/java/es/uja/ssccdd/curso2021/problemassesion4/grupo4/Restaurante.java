/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo4;

import es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TIEMPO_ESPERA_MAX;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TIEMPO_ESPERA_MIN;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TOTAL_PLATOS_POR_TIPO;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Restaurante implements Runnable {

    private final int iD;
    private final Utils.TipoPlato tipoPlato;
    private final ArrayList<Plato> platosCompartidos;
    private final ReentrantLock cerrojo;

    public Restaurante(int iD, TipoPlato tipoPlato, ArrayList<Plato> platosCompartidos, ReentrantLock cerrojo) {
        this.iD = iD;
        this.tipoPlato = tipoPlato;
        this.platosCompartidos = platosCompartidos;
        this.cerrojo = cerrojo;
    }

    @Override
    public void run() {

        System.out.println("Restaurante " + iD + " ha empezado.");

        while (platosCompartidos.size() < TOTAL_PLATOS_POR_TIPO) {

            cerrojo.lock();
            {
                if (platosCompartidos.size() < TOTAL_PLATOS_POR_TIPO) {
                    //Doble check en región crítica para asegurarse que hay hueco
                    platosCompartidos.add(new Plato(Math.abs(random.nextInt()), tipoPlato));
                }
            }
            cerrojo.unlock();

            int tiempoEspera = random.nextInt(TIEMPO_ESPERA_MAX - TIEMPO_ESPERA_MIN) + TIEMPO_ESPERA_MIN;
            try {
                TimeUnit.MILLISECONDS.sleep(tiempoEspera);
            } catch (InterruptedException ex) {
                //Los hilos no van a ser interrumpidos
                Logger.getLogger(Restaurante.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.out.println("Restaurante " + iD + " ha terminado.");

    }

}
