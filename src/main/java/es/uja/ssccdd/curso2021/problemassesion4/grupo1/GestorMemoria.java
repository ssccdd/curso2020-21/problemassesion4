/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.MEMORIA_COMPLETA;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.NUM_FALLOS;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.TIPOS_PROCESO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.aleatorio;
import es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Sesion4.generaID_SISTEMA;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorMemoria implements Callable<Informe> {
    private final String iD;
    private final ArrayList<Ordenador> listaOrdenadores;
    private final ArrayList<Proceso> noAsignados;
    private int ultimoOrdenador; // último ordenador donde se asignó un proceso
    private int fallosAsignacion;
    private final ExecutorService ejecucion;
    private List<Future<Proceso>> listaProcesosCreados;

    public GestorMemoria(String iD) {
        this.iD = iD;
        this.listaOrdenadores = new ArrayList();
        this.noAsignados = new ArrayList();
        this.ultimoOrdenador = 0;
        this.fallosAsignacion = 0;
        
        // Marco ejecución para crear los procesos
        this.ejecucion = Executors.newCachedThreadPool();

    }

    @Override
    public Informe call() throws Exception {
        Informe informe = new Informe(iD);
        
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        inicializaGestor();
        
        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while( fallosAsignacion < NUM_FALLOS ) {
                crearProcesos();
                
                asignarProcesos();
            }
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException | ExecutionException ex) {
            informe.setCancelado(true);
            System.out.println("Se ha interrumpido la ejecución del TAREA-" + iD + ex);
        } finally {
            // Completa la finbalización del gestor
            finalizacion(informe);
            // Finalizamos la ejecución de tareas pendientes
            ejecucion.shutdownNow();
            ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        }
        
        return informe;
    }

    private void inicializaGestor() {
        int[] capacidad = new int[TIPOS_PROCESO];
        
        int totalOrdenadores = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        for(int j = 0; j < totalOrdenadores; j++) {
            // Generamos la capacidad del ordenador
            for(int k = 0; k < TIPOS_PROCESO; k++) 
                capacidad[k] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
            listaOrdenadores.add(new Ordenador(generaID_SISTEMA(),capacidad));
        }
    }
    
    private void crearProcesos() throws InterruptedException {
        ArrayList<CrearProceso> listaTareas;
        
        // Creamos las tareas para la obtener los procesos
        int numTareas = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        listaTareas = new ArrayList();
        for(int i = 0; i < numTareas; i++) {
            TipoProceso tipoProceso = TipoProceso.getTipoProceso(aleatorio.nextInt(VALOR_CONSTRUCCION));
            CrearProceso tarea = new CrearProceso(iD + "-tarea para proceso- " + i, tipoProceso);
            listaTareas.add(tarea);
        }
        
        // Ejecutamos las tareas y esperamos hasta que finalicen todas
        listaProcesosCreados = ejecucion.invokeAll(listaTareas);
    }
    
    private void asignarProcesos() throws InterruptedException, ExecutionException {
        // Asignamos los procesos a los ordenadores
        Iterator it = listaProcesosCreados.iterator();
        while( it.hasNext() && (fallosAsignacion < NUM_FALLOS)) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de proceso.
            if ( Thread.interrupted() )
                throw new InterruptedException();
            
            Future<Proceso> procesoCreado = (Future<Proceso>) it.next();
            it.remove(); // Eliminamos el proceso de la lista
            Proceso proceso = procesoCreado.get();
            System.out.println("TAREA-" + iD + " Asignando " + proceso + " faltan " + listaProcesosCreados.size());
                
            // Asignamos el proceso a un ordenador si es posible
            boolean asignado = false;
            int ordenadoresRevisados = 0;
            while( (ordenadoresRevisados < listaOrdenadores.size()) && !asignado ) {
            int indice = (ultimoOrdenador + ordenadoresRevisados) % listaOrdenadores.size();
            if(listaOrdenadores.get(indice).addProceso(proceso) != MEMORIA_COMPLETA)
                asignado = true;
            else
                ordenadoresRevisados++;
            }
            
            // Si no se ha asignado se anota el proceso en otro caso se actualiza
            // el último ordenador asignado
            if(asignado)
                ultimoOrdenador = (ultimoOrdenador + ordenadoresRevisados + 1) % listaOrdenadores.size();
            else {
                noAsignados.add(proceso);
                fallosAsignacion++;
            }
            
            // Simulamos por último el tiempo de asignación porque si se interrumpe
            // ya hemos completado la operación de asignación
            TimeUnit.SECONDS.sleep(tiempoAsignacion(proceso.getTipoProceso()));
        }
    }
    
    private void finalizacion(Informe informe) {
        // registramos los procesos no asignados
        try {
            for(Future<Proceso> procesoCreado : listaProcesosCreados)
                noAsignados.add(procesoCreado.get());

        } catch (InterruptedException | ExecutionException ex) {
            // No hacemos nada porque se está finializando
        } finally {
           informe.setListaOrdenadores(listaOrdenadores);
           informe.setListaNoAsignados(noAsignados);
        }
    }
    
    public String getiD() {
        return iD;
    }

    private int tiempoAsignacion(TipoProceso tipoProceso) {
        return MAXIMO + tipoProceso.ordinal();
    }
}
