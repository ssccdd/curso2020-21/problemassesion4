/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Sesion4.generaID_SISTEMA;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo2.Constantes.TipoPrioridad.getTipoPrioridad;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class CrearProceso implements Callable<Proceso> {
    private final String iD;

    public CrearProceso(String iD) {
        this.iD = iD;
    }

    @Override
    public Proceso call() throws Exception {
        Proceso proceso = null;
        
        try {
            
            proceso = crearProceso();
            
            System.out.println("TAREA-" + iD + " Crea un proceso");
            
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " Ha sido CANCELADO " + ex);
        } 
        
        return proceso;
    }
    
    private Proceso crearProceso() throws InterruptedException {
        // Simula la creación de un proceso
        int construccion = aleatorio.nextInt(VALOR_CONSTRUCCION);
        Proceso proceso = new Proceso(generaID_SISTEMA(), getTipoPrioridad(construccion));
        int tiempo = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        TimeUnit.SECONDS.sleep(tiempo);
        
        return proceso;

    }

    public String getiD() {
        return iD;
    }
}
