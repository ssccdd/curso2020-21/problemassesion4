/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TOTAL_TIPOS_PLATOS;
import java.util.Arrays;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MenuReparto {

    private final int iD;
    private final Plato[] platos;

    /**
     * Crea un MenuReparto sin platos asignados
     *
     * @param iD identificador asociado al pedido
     */
    public MenuReparto(int iD) {
        this.platos = new Plato[TOTAL_TIPOS_PLATOS];

        this.iD = iD;
    }

    public int getiD() {
        return iD;
    }

    public Plato[] getPlatos() {
        return platos;
    }

    /**
     * Asigna un nuevo plato al pedido
     *
     * @param plato para añadir al pedido
     * @return true si se ha añadido false si ya tenía ese tipo de plato
     */
    public boolean addPlato(Plato plato) {
        boolean resultado = false;

        if (platos[plato.getTipo().ordinal()] == null) {
            platos[plato.getTipo().ordinal()] = plato;
            resultado = true;
        }

        return resultado;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        String resultado;

        resultado = "Pedido[" + iD + ", estado='" + (pedidoCompleto() ? "LISTO" : "NO_COMPLETO") + "']{" + "Platos=" + Arrays.toString(platos) + '}';

        return resultado;
    }

    /**
     * Comprueba si el pedido esta completo
     *
     * @return true si esta completo, false si no
     */
    private boolean pedidoCompleto() {
        boolean completo = true;
        int i = 0;

        while ((i < platos.length) && completo) {
            if (platos[i] == null) {
                completo = false;
            }
            i++;
        }

        return completo;
    }

}
