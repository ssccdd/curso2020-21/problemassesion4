/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.TipoArchivo.getTipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo3.Sesion4.generaID_SISTEMA;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class CrearArchivo implements Callable<Archivo> {
    private final String iD;

    public CrearArchivo(String iD) {
        this.iD = iD;
    }

    @Override
    public Archivo call() throws Exception {
        Archivo archivo = null;
        
        try {
            
            archivo = crearArchivo();
            
            System.out.println("TAREA-" + iD + " Crea un archivo");
            
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " Ha sido CANCELADO " + ex);
        } 
        
        return archivo;
    }
    
    private Archivo crearArchivo() throws InterruptedException {
        // Simula la creación de un proceso
        int construccion = aleatorio.nextInt(VALOR_CONSTRUCCION);
        Archivo archivo = new Archivo(generaID_SISTEMA(), getTipoArchivo(construccion));
        int tiempo = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        TimeUnit.SECONDS.sleep(tiempo);
        
        return archivo;
    }

    public String getiD() {
        return iD;
    }
}
