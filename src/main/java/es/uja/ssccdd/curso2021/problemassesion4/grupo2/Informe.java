/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo2;

import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Informe {
    private final String gestor;
    private ArrayList<ColaPrioridad> listaColasPrioridad;
    private ArrayList<Proceso> listaNoAsignados;
    private boolean cancelado;

    public Informe(String gestor) {
        this.gestor = gestor;
        this.listaColasPrioridad = null;
        this.listaNoAsignados = null;
        this.cancelado = false;
    }

    public Informe(String gestor, ArrayList<ColaPrioridad> listaColasPrioridad, ArrayList<Proceso> listaNoAsignados) {
        this.gestor = gestor;
        this.listaColasPrioridad = listaColasPrioridad;
        this.listaNoAsignados = listaNoAsignados;
        this.cancelado = false;
    }

    public void setListaColasPrioridad(ArrayList<ColaPrioridad> listaColasPrioridad) {
        this.listaColasPrioridad = listaColasPrioridad;
    }

    public void setListaNoAsignados(ArrayList<Proceso> listaNoAsignados) {
        this.listaNoAsignados = listaNoAsignados;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }
    
    @Override
    public String toString() {
        String resultado = "******************** " + gestor + " ********************\n";
        
        for(ColaPrioridad colaPrioridad : listaColasPrioridad)
                resultado = resultado + colaPrioridad + "\n";
            
        resultado = resultado + "Procesos no asignados " + listaNoAsignados + "\n";

        if( cancelado )
            resultado = "El gestor ha sido CANCELADO\n";
        
        return resultado;
    }
}
