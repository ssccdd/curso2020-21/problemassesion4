/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TIEMPO_ESPERA_MAX;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TIEMPO_ESPERA_MIN;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.TOTAL_TIPOS_PLATOS;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo4.Utils.random;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Repartidor implements Runnable {

    private final int iD;
    private final ArrayList<MenuReparto> pedidos;
    private final ArrayList<ReentrantLock> cerrojos;
    private final ArrayList<ArrayList<Plato>> platos;

    public Repartidor(int iD, ArrayList<MenuReparto> pedidos, ArrayList<ReentrantLock> cerrojos, ArrayList<ArrayList<Plato>> platos) {
        this.iD = iD;
        this.pedidos = pedidos;
        this.cerrojos = cerrojos;
        this.platos = platos;
    }

    @Override
    public void run() {
        
        System.out.println("Repartidor " + iD + " ha empezado.");

        for (int i = 0; i < pedidos.size(); i++) {

            for (int j = 0; j < TOTAL_TIPOS_PLATOS; j++) {

                Plato plato;
                cerrojos.get(j).lock();
                {
                    plato = platos.get(j).remove(0);
                }
                cerrojos.get(j).unlock();

                pedidos.get(i).addPlato(plato);

            }

            int tiempoEspera = random.nextInt(TIEMPO_ESPERA_MAX - TIEMPO_ESPERA_MIN) + TIEMPO_ESPERA_MIN;
            
            try {
                TimeUnit.MILLISECONDS.sleep(tiempoEspera);
            } catch (InterruptedException ex) {
                //Los hilos no van a ser interrumpidos
                Logger.getLogger(Repartidor.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        imprimirDatos();

    }

    /**
     * Imprimr la información del hilo
     *
     * @return Cadena de texto con la información
     */
    private void imprimirDatos() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nRepartidor ").append(iD);

        for (MenuReparto pedido : pedidos) {
            mensaje.append("\n\t").append(pedido.toString());
        }

        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
