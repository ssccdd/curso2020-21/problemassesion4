/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.TIEMPO_ESPERA_INGENIERO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.MODELOS_A_GENERAR_MAX;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo5.Utils.MODELOS_A_GENERAR_MIN;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ingeniero implements Callable<Integer> {

    private final int iD;
    private final Impresora3D impresora;
    private final ArrayList<Modelo> modelosImpresos;
    private final ReentrantLock bloqueoModelosImpresos;

    public Ingeniero(int iD, Impresora3D impresora, ArrayList<Modelo> modelosImpresos, ReentrantLock bloqueoModelosImpresos) {
        this.iD = iD;
        this.impresora = impresora;
        this.modelosImpresos = modelosImpresos;
        this.bloqueoModelosImpresos = bloqueoModelosImpresos;
    }

    @Override
    public Integer call() throws Exception {

        System.out.println("El ingeniero " + iD + " ha comenzado el trabajo.");
        int contador = 0;

        int numeroModelos = random.nextInt(MODELOS_A_GENERAR_MAX - MODELOS_A_GENERAR_MIN) + MODELOS_A_GENERAR_MIN;

        for (int i = 0; i < numeroModelos; i++) {

            Modelo m = new Modelo(iD, impresora.getCalidadImpresion());

            bloqueoModelosImpresos.lock();
            {
                modelosImpresos.add(m);
            }
            bloqueoModelosImpresos.unlock();

            contador++;
            try {
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_INGENIERO);
            } catch (InterruptedException ex) {
                //No hacemos nada porque el hilo no se interrumpirá.
                Logger.getLogger(Ingeniero.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("El ingeniero " + iD + " ha terminado el trabajo.\tTotal modelos impresos: " + contador);
        
        return contador;
    }

}
