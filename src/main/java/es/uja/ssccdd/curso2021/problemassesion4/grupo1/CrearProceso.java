/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion4.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.MINIMO;
import es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Constantes.aleatorio;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion4.grupo1.Sesion4.generaID_SISTEMA;

/**
 *
 * @author pedroj
 */
public class CrearProceso implements Callable<Proceso> {
    private final String iD;
    private final TipoProceso tipoProceso;

    public CrearProceso(String iD, TipoProceso tipoProceso) {
        this.iD = iD;
        this.tipoProceso = tipoProceso;
    }

    @Override
    public Proceso call() throws Exception {
        Proceso proceso = null;
        
        try {
            
            proceso = crearProceso();
            
            System.out.println("TAREA-" + iD + " Crea un proceso");
            
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " Ha sido CANCELADO " + ex);
        } 
        
        return proceso;
    }
    
    private Proceso crearProceso() throws InterruptedException {
        // Simula la creación de un proceso
        Proceso proceso = new Proceso(generaID_SISTEMA(), tipoProceso);
        int tiempo = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        TimeUnit.SECONDS.sleep(tiempo);
        
        return proceso;

    }

    public String getiD() {
        return iD;
    }
}
